//
//  ViewController.swift
//  hw2_calculator
//
//  Created by art on 10/04/2017.
//  Copyright © 2017 art. All rights reserved.
//

import UIKit

extension Double {
    
    /// This computed property would provide a formatted string representation of this double value.
    /// For an integer value, like `2.0`, this property would be `"2"`.
    /// And for other values like `2.4`, this would be `"2.4"`.
    fileprivate var displayString: String {
        // 1. We have to check whether this double value is an integer or not.
        //    Here I subtract the value with its floor. If the result is zero, it's an integer.
        //    (Note: `floor` means removing its fraction part, 無條件捨去.
        //           `ceiling` also removes the fraction part, but it's by adding. 無條件進位.)
        let floor = self.rounded(.towardZero)  // You should check document for the `rounded` method of double
        let isInteger = self.distance(to: floor).isZero
        
        let string = String(self)
        if isInteger {
            // Okay this value is an integer, so we have to remove the `.` and tail zeros.
            // 1. Find the index of `.` first
            if let indexOfDot = string.characters.index(of: ".") {
                // 2. Return the substring from 0 to the index of dot
                //    For example: "2.0" --> "2"
                return string.substring(to: indexOfDot)
            }
        }
        // Return original string representation
        return String(self)
    }
}

class ViewController: UIViewController {

    var core = Core<Double>()
    
    @IBOutlet weak var displayNumber: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        displayNumber.text = "0";
        
//        displayNumber.text = "00";
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func acClicked(_ sender: Any) {
        displayNumber.text = "0";
        core = Core<Double>();
    }

    @IBAction func dotClicked(_ sender: Any) {
        guard !(displayNumber.text?.contains("."))! else{
            return;
        }
        displayNumber.text = displayNumber.text! + ".";
    }
    

    @IBAction func operatorClicked(_ sender: UIButton) {
                let currentNumber = Double(displayNumber.text!)!
        try! core.addStep(currentNumber);

        displayNumber.text = "0";

        switch sender.tag {
        case 3000: // Add
            try! core.addStep(+);
        case 3001: // Sub
            try! core.addStep(-);
        case 3002: // Sub
            try! core.addStep(*);
        case 3003: // Sub
            try! core.addStep(/);
        default:
            fatalError("Unknown operator button: \(sender)")
        }
    }
    
    @IBAction func percentClicked(_ sender: Any) {
          var currentNumber = Double(displayNumber.text ?? "0")!
        
        currentNumber = currentNumber/100;
        
        displayNumber.text = currentNumber.displayString;
    }
    @IBAction func logClicked(_ sender: Any) {
         var currentNumber = Double(displayNumber.text ?? "0")!
        currentNumber = log(currentNumber)/log(10)
    
        displayNumber.text = currentNumber.displayString;
    }

    @IBAction func expClicked(_ sender: Any) {
        var currentNumber = Double(displayNumber.text ?? "0")!;
        
        currentNumber = exp(currentNumber);
        
        displayNumber.text = currentNumber.displayString;
    }
    @IBAction func piClicked(_ sender: Any) {
        displayNumber.text = String(Float.pi);
    }
    @IBAction func calculateClicked(_ sender: Any) {
       
        let currentNumber = Double(displayNumber.text ?? "0")!
        try! core.addStep(currentNumber)
        
        let result = core.calculate()!
        displayNumber.text = result.displayString;
        
        core = Core<Double>()
    }

    
    @IBAction func numberBtClicked(_ sender: Any) {
        let button:UIButton = sender as! UIButton;
        var number:Int;
        
        if(button.tag == 2000){
            if(displayNumber.text=="0"){
            }else{
            displayNumber.text = displayNumber.text! + "00";
            }
        }else{
            number = button.tag-1000;
            if(displayNumber.text=="0"){
                       displayNumber.text = String(number);
            }else{
                 displayNumber.text = displayNumber.text! + String(number);

            }
        }
        
   
    }
    

}

